package oy.tol.tra;

import java.util.Arrays;
/**
 * For finding the mode of a number from an array of integers.
 * 
 * Usage: Mode.Result foundMode = Mode.findMode(array);
 * 
 * Result contains the number most often found in the array,
 * as well as the count of the said number.
 */
public class Mode {

   public static class Result {
      public int number = 0;
      public int count = 0;
   }

   private Mode() {
      // Empty
   }

   /**
    * Finds the mode of the specified array, -1 if one could not be found. Mode
    * means the number most often found in the array.
    * 
    * @return The Result, containing the mode number and the count how many time it was in the array.
    */
   public static Result findMode(int [] array) {
       // TODO: implement this to find the mode of the integer array.
       
       Arrays.sort(array);
       
       Result result = new Result();
       result.number = array[0];
       result.count = 0;
       
       int num = array[0];
       int count = 0;
       
       int len = array.length;
       
       for(int i = 0; i < len; i++){
           int it = array[i];
           if(it != num){ //Not the same number we were counting
               if(count > result.count){
                   result.number = num;
                   result.count = count;
               }
               num = it;
               count = 0;
           }
           count ++;
       }
       //Also check the last number
       if(count > result.count){
           result.number = num;
           result.count = count;
       }
       
       return result;
       
   }
}
