package oy.tol.tra;

import java.lang.reflect.Array;

public class QueueImplementation<E> implements QueueInterface<E> {
    private int head;
    private int count;
    private int capacity;
    private E [] itemArray;

    
    @Override
    public void init(Class<E> elementClass, int size) throws QueueAllocationException{
        this.capacity = size;
        this.count = 0;
        try{
            this.itemArray = (E []) Array.newInstance(elementClass, size);
        }catch (OutOfMemoryError e){
            throw new QueueAllocationException("Failed to allocate array");
        }
    }
    
    @Override
    public int capacity(){
        return this.capacity;
    }
    
    @Override
    public void enqueue(E element) throws QueueAllocationException, NullPointerException{
        if(element == null)
            throw new NullPointerException("Element is null");

        if(this.count == this.capacity){
            try{
                int firstCount = this.capacity - this.head;
                
                this.capacity <<= 2;
                E expanded[] = (E []) Array.newInstance(element.getClass() , this.capacity);
                
                System.arraycopy(this.itemArray, this.head, expanded, 0, firstCount);
                System.arraycopy(this.itemArray, 0, expanded, firstCount, this.count - firstCount);
                
                this.itemArray = expanded;
                this.head = 0;
                
            }catch(OutOfMemoryError e){
                throw new QueueAllocationException("Failed to allocate array");
            }
        }
        this.itemArray[(this.head + this.count) % this.capacity] = element;
        this.count ++;
    }

    @Override
    public E dequeue() throws QueueIsEmptyException{
        if(this.count == 0){
            throw new QueueIsEmptyException("Queue is empty");
        }
        E element = this.itemArray[this.head];
        this.head = (this.head + 1) % this.capacity;
        this.count --;
        
        return element; 
    }
    
    @Override
    public E element() throws QueueIsEmptyException{
        if(this.count == 0){
            throw new QueueIsEmptyException("Queue is empty");
        }
        return this.itemArray[this.head];
    }

    @Override
    public int count(){
        return this.count;
    }

    @Override
    public boolean empty(){
        return this.count == 0;
    }
    
    @Override
    public void reset(){
        this.count = 0;
        this.head = 0;
    }
    
    @Override
    public String toString(){
        if(this.count == 0){
            return "[]";
        }

        String s = "[";
        s += String.format("%d", this.itemArray[(this.head) % this.capacity]);
        
        for(int i = 1; i < this.count; i++){
            s += String.format(", %d", this.itemArray[(this.head + i) % this.capacity]);
        }
        
        s += "]";
        return s;
    }
}
