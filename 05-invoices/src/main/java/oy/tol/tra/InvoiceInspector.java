package oy.tol.tra;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.*;

public class InvoiceInspector {

    /** List of invoices sent to customer. */
    Invoice[] invoices = null;
    /** List of payments received from customers. */
    Payment[] payments = null;
    /**
     * Based on invoices and payments, a list of new invoices to be sent to
     * customers. DO NOT use Java containers in your implementation, it is used ONLY here
     * to store the invoices to collect. Use plain Java arrays {@code Invoice[]} and {@code Payment[]} 
     */
    List<Invoice> toCollect = new ArrayList<>();

    /**
     * Reads the invoices and payments to memory from csv text files.
     * 
     * @param invoicesFile The file containing the invoice data.
     * @param paymentsFile The file containing the payments data.
     * @throws IOException If file thing goes wrong, there will be exception.
     */
    public void readInvoicesAndPayments(String invoicesFile, String paymentsFile) throws IOException {
        BufferedReader invoiceReader = new BufferedReader(new FileReader(invoicesFile));
        String line = null;
        line = invoiceReader.readLine();
        int itemCount = 0;
        if (null != line && line.length() > 0) {
            itemCount = Integer.parseInt(line);
            invoices = new Invoice[itemCount];
        } else {
            invoiceReader.close();
            throw new IOException("Could not read the invoice file");
        }
        itemCount = 0;
        while ((line = invoiceReader.readLine()) != null && line.length() > 0) {
            String[] items = line.split(",");
            invoices[itemCount++] = new Invoice(Integer.parseInt(items[0]), Integer.parseInt(items[1]));
        }
        invoiceReader.close();
        BufferedReader paymentsReader = new BufferedReader(new FileReader(paymentsFile));
        line = paymentsReader.readLine();
        itemCount = 0;
        if (null != line && line.length() > 0) {
            itemCount = Integer.parseInt(line);
            payments = new Payment[itemCount];
        } else {
            paymentsReader.close();
            throw new IOException("Could not read the invoice file");
        }
        itemCount = 0;
        while ((line = paymentsReader.readLine()) != null && line.length() > 0) {
            String[] items = line.split(",");
            payments[itemCount++] = new Payment(Integer.parseInt(items[0]), Integer.parseInt(items[1]));
        }
        paymentsReader.close();
    }

    /**
     * A naive simple implementation handling the creation of new invoices based on
     * old invoices and payments received from customers.
     * 
     * @throws IOException
     */
    public void handleInvoicesAndPaymentsSlow() throws IOException {
        for (int counter = 0; counter < invoices.length; counter++) {
            Invoice invoice = invoices[counter];
            boolean noPaymentForInvoiceFound = true;
            for (int paymentCounter = 0; paymentCounter < payments.length; paymentCounter++) {
                Payment payment = payments[paymentCounter];
                if (invoice.number.compareTo(payment.number) == 0) {
                    noPaymentForInvoiceFound = false;
                    if (invoice.sum.compareTo(payment.sum) > 0) {
                        toCollect.add(new Invoice(invoice.number, invoice.sum - payment.sum));
                        break;
                    }
                }
            }
            if (noPaymentForInvoiceFound) {
                toCollect.add(invoice);
            }
        }
        Collections.sort(toCollect);
    }

    public void saveNewInvoices(String outputFile) throws IOException {
        BufferedWriter toCollectWriter = new BufferedWriter(new FileWriter(outputFile));
        for (Invoice invoice : toCollect) {
            toCollectWriter.write(invoice.number.toString() + "," + invoice.sum.toString());
            toCollectWriter.newLine();
        }
        toCollectWriter.close();
    }
    public static void radixSort_256(Identifiable[] arr){
        int[] counts = new int[256];
        int[] prefixSum = new int[256];

        Identifiable[] swapArray = new Identifiable[arr.length];
        for(int b = 0; b < Integer.SIZE / 8; b++){
            Arrays.fill(counts, 0);
            for(Identifiable i : arr){
                counts[(i.getID() >> (b * 8)) & 0xFF] ++;
            }

            int sum = 0;
            for(int i = 0; i < 256; i++){
                prefixSum[i] = sum;                
                sum += counts[i];
            }
            
            Arrays.fill(counts, 0);
            for(int i = 0; i < arr.length; i++){
                int index = (arr[i].getID() >> (b * 8)) & 0xFF;
                swapArray[prefixSum[index] + counts[index]] = arr[i];
                counts[index]++;
            }
            
            Identifiable[] temp = arr;
            arr = swapArray;
            swapArray = temp;
        }
        
    }
    public static int binarySearch_IdentifiableID(int id, Identifiable[] arr, int start, int end){
        for(;;){
            int middle = start + (end - start) / 2;   
            
            int it = arr[middle].getID();
            if(it == id) return middle;

            if(middle == start)
                return -1;
            
            if(id > it) start = middle;
            else end = middle;
        }
    }

    /**
     * TODO: Implement this method so that the time complexity of creating new
     * invoices to clients is significantly faster than the naive
     * handleInvoicesAndPaymentsSlow().
     * How to do this:
     * 1. Both invoices and payments need to be sorted, so implement sorting with any method.
     * 2. When going through invoices, search for the corresponding payment using binary search, so implement that.
     * 3. If a payment was found, deduct from the invoice what was paid. If must still pay something, create invoice.
     * 4. If payment was not found, create a new invoice with the same invoice.
     * @throws IOException
     */
    public void handleInvoicesAndPaymentsFast(){
        radixSort_256(invoices);
        radixSort_256(payments);
        
        for(Invoice invoice : invoices){
            
            int index = binarySearch_IdentifiableID(invoice.getID(), payments, 0, payments.length);
            if(index == -1){
                toCollect.add(new Invoice(invoice.getID(), invoice.sum));
                continue;
            }
            Payment payment = payments[index];
            if(payment.sum >= invoice.sum) continue;
            
            toCollect.add(new Invoice(invoice.getID(), invoice.sum - payment.sum));
        }
    }
    
}
