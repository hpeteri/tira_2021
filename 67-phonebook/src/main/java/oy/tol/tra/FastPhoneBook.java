package oy.tol.tra;

public class FastPhoneBook implements PhoneBook {
    
    static final int MAX_TABLE_SIZE = 300000;
    //99853
    private Person [] persons = new Person[MAX_TABLE_SIZE];
    private int count = 0;
    private int tryCount_worst = 0; //How many tries it takes to find chained element
    private int tryCount = 0; //Collisions in total
    
    /**
     * TODO: Implement the add depending on your selected data structure; either hash table or BST.
     * In both cases, you need to calculate the hash code in the Person class.
     * 
     * @param person The person to add to the phone book.
     * @return True if managed to add the person, otherwise false.
     */
    @Override
    public boolean add(Person person) throws IllegalArgumentException {
        if(person == null)
            throw new IllegalArgumentException("Trying to add null");

        if(this.count == MAX_TABLE_SIZE)
            return false;

        int hash = person.hashCode();
        int index = hash % MAX_TABLE_SIZE;
        int tries = 0;
        
        for(;;index = (index + 1) % MAX_TABLE_SIZE){
            Person it = persons[index];
            if(it == null){
                persons[index] = person;
                this.count ++;
                return true;
            }
            if(it.equals(person)){
                return false;
            }

            tries ++;
            this.tryCount ++;
            this.tryCount_worst = Math.max(tries, this.tryCount_worst);
        }
    }

    @Override
    public int size() {
        return this.count;
    }

    /**
     * TODO: Implement the search function.
     * Implementation depends on which data structure you use, either hash table or BST.
     *
     * @param number The phone number to search from the phone book.
     * @return Return the Person object, or if not found null.
     */
    @Override
    public Person findPersonByPhone(String number) throws IllegalArgumentException {
        if(number == null)
            throw new IllegalArgumentException("Trying to find null");
        
        int hash = Person.stringHash(number);
        int index = hash % MAX_TABLE_SIZE;
        int tries = 0;
        
        for(;tries < MAX_TABLE_SIZE ;index = (index + 1) % MAX_TABLE_SIZE){
            Person it = persons[index];
            if(it == null){
                return null;
            }
            
            if(it.getPhoneNumber().equals(number)){
                return it;
            }
            tries ++;
            
            this.tryCount ++;
            this.tryCount_worst = Math.max(tries, this.tryCount_worst);
        }
        return null;
    }

    @Override
    public Person[] getPersons() {
        // Students: You do not need to implemented this here.
        // Just let it return null.
        return null;
    }

    /**
     * Prints out the statistics of the phone book.
     * Here you should print out member variable information which tell something about
     * your implementation.
     * <p>
     * For example, if you implement this using a hash table, update member variables of the class
     * (int counters) in add(Person) whenever a collision happen. Then print this counter value here. 
     * You will then see if you have too many collisions. It will tell you that your hash function
     * is not good. In the teacher's implementation, there were 864 collisions and when using 
     * linear probing to handle the collision, max number of steps to find a free slot
     * in the table was 2. This is not too bad.
     */
    @Override
    public void printStatus(){      
        System.out.println(String.format("Tries in total :: %d\nWorst case when looking or adding chained element :: %d", this.tryCount, this.tryCount_worst));
    }
    
}
