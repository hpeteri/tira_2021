package oy.tol.tra;

import java.lang.reflect.Array;

/**
 * An implementation of the StackInterface.
 * <p>
 * TODO: Students, implement this so that the tests pass.
 */
public class StackImplementation<E> implements StackInterface<E> {

    private E [] itemArray;
    private int capacity;
    private int currentIndex = -1;

    @Override
    public void init(Class<E> elementClass, int size) throws StackAllocationException {
        if (size < 2) {
            throw new StackAllocationException("Stack size should be greater than 1");
        }
        try {
                
            capacity = size;
            currentIndex = -1;
            itemArray = (E []) Array.newInstance(elementClass, capacity);   
        } catch (Exception e) {
            throw new StackAllocationException(e.getMessage());
        }
    }

    @Override
    public int capacity() {
        return this.capacity;
    }

    @Override
    public void push(E element) throws StackAllocationException, NullPointerException {
        if(element == null){
            throw new NullPointerException("Element is null");
        }
        if(this.currentIndex == this.capacity - 1){
            try{
                this.capacity <<= 2;
                E expanded[] = (E []) Array.newInstance(element.getClass() , this.capacity);
                System.arraycopy(this.itemArray, 0, expanded, 0, this.count());
                this.itemArray = expanded;
                                
            }catch(OutOfMemoryError e){
                throw new StackAllocationException("Failed to allocate itemArray");
            }
        }
        
        this.itemArray[++this.currentIndex] = element;
        
    }

    @Override
    public E pop() throws StackIsEmptyException {
        if(this.count() == 0){
            throw new StackIsEmptyException("Trying to pop from empty stack");
        }        
        return this.itemArray[this.currentIndex--];
        
    }

    @Override
    public E peek() throws StackIsEmptyException {
        if(this.count() == 0){
            throw new StackIsEmptyException("Trying to pop from empty stack");
        }
        return this.itemArray[this.currentIndex];
    }

    @Override
    public int count() {
        return this.currentIndex + 1;
    }

    @Override
    public void reset() {
        this.currentIndex = -1;
    }

    @Override
    public boolean empty() {
        return this.count() == 0;
    }

}
