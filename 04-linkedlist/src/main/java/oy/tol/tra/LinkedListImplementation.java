package oy.tol.tra;

public class LinkedListImplementation<Element> implements LinkedListInterface<Element> {

   private class Node<E> {
      Node(E data) {
         element = data;
         next = null;
      }
      E element;
      Node<E> next;
   }

   private Node<Element> head = null;
   private int size = 0;

   @Override
   public void add(Element element) throws NullPointerException, LinkedListAllocationException {
       if(element == null)
           throw new NullPointerException("Element is null");

       Node<Element> node = null;
       try{
           node = new Node<Element>(element);
           this.size ++;
       }catch (OutOfMemoryError e){
           throw new LinkedListAllocationException("Failed to allocate new node");
       }
       
       if(this.head == null){
           this.head = node;
           return;
       }

       Node<Element> at = this.head;
       for(int i = 0; i < this.size -1; i++){
           
           if(at.next == null){
               at.next = node;
               return;
           }
           at = at.next;
       }
   }

   @Override
   public void add(int index, Element element) throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
       if(element == null)
           throw new NullPointerException("Element is null");
       if(index < 0 || index > this.size)
           throw new IndexOutOfBoundsException("Index out of range");

       Node<Element> node = null;
       try{
           node = new Node<Element>(element);
           this.size ++;
       }catch (OutOfMemoryError e){
           throw new LinkedListAllocationException("Failed to allocate new node");
       }

       if(index == 0){
           node.next = this.head;
           this.head = node;           
           return;
       }
       
       Node<Element> prev = null;
       Node<Element> at = this.head;
       for(int i = 0; i < index; i++){
           prev = at;
           at = at.next;
       }
       prev.next = node;
       node.next = at;           
       
   }

   @Override
   public boolean remove(Element element) throws NullPointerException {
       if(element == null)
           throw new NullPointerException("Element is null");
       
       if(this.head == null){
           return false;
       }
       
       if(element == this.head.element){
           this.head = this.head.next;
           this.size --;
           return true;
       }
       
       Node<Element> prev = this.head;
       for(Node<Element> at = this.head.next; at != null; at = at.next){
           if(at.element == element){
               prev.next = at.next;
               this.size --;
               return true;
           }
       }
      return false;
   }

   @Override
   public Element remove(int index) throws IndexOutOfBoundsException {
       if(index < 0 || index >= this.size)
           throw new IndexOutOfBoundsException("Index out of range");

       if(index == 0){
           Element result = this.head.element;
           this.head = this.head.next;
           size --;
           return result;
       }

       Node<Element> prev = this.head;
       Node<Element> at = this.head.next;
       for(int i = 1; i < index; i++){
           prev = at;
           at = at.next;
       }
       prev.next = at.next;
       this.size --;
       return at.element;
   }

   @Override
   public Element get(int index) throws IndexOutOfBoundsException {
       if(index < 0 || index >= this.size)
           throw new IndexOutOfBoundsException("Index out of range");

       Node<Element> at = this.head;
       for(int i = 0; i < index; i++){
           at = at.next;
       }
       if(at == null){
           System.out.println("Got null from get");
       }
       return at.element;
   }

   @Override
   public int indexOf(Element element) throws NullPointerException{
       if(element == null)
           throw new NullPointerException("Element is null");

       Node<Element> at = this.head;
       for(int i = 0; i < this.size; i++){
           if(at.element == element)
               return i;

           at = at.next;
       }
       return -1;
   }

   @Override
   public int size() {
       return this.size;
   }

   @Override
   public void reset() {
       this.head = null;
       this.size = 0;
   }
    
   @Override
   public void reverse() {
       // TODO: implement this only when doing the task explained the TASK-2.md.
       // This method is not needed in doing the task in the README.md.
       if(this.head == null)
           return;

       Node<Element> newHead = this.head;
       Node<Element> at = this.head.next;
       newHead.next = null;
       
       for(;at != null;){
           Node<Element> temp = at.next;
           at.next = newHead;
           newHead = at;
           at = temp;
       }

       this.head = newHead;       
   }
   
}
