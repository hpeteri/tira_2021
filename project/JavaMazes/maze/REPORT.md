# Report 

## Implementations

### BFS, BreadthFirstSearch
    
    add start to queue
    for each vertex in queue
        if vertex == target:
           stop
        for each destination not visited from vertex:
            add destination to queue
            add destination to visited


Not much thought was given which datastructure variant should be used to achieve the best performance.
Just needed the basic functionality for accessing vertices in correct order.


Visited vertices could have been copied first from the graph and removed when they were visited. 
currently HashSet starts as empty, and vertices are added as they are visited.
If performance would be measured, this would be a point of interest, since there might a performance difference.
- If target is found early, it might be faster to start off empty, since less collisions in the hashset
- not copying all vertices to the hashset first might be slower due to memory copies and hashset expansion.

    
### DFS, DepthFirstSearch

Same implementation as BFS, but vertices are stored in a stack instead of a queue since vertices need to be accessed in a different order.

### isDisconnected

DFS with a slight modification
         
    for each vertex in graph
        add vertex to toVisit

    start = toVisit.first()
    remove start from toVisit
    add start to stack
           
    for each vertex in stack
        for each destination in toVisit from vertex:
            add destination to stack
            remove destination from toVisit


    return toVisit not empty:

if every vertex could not have been visited from starting vertex, then graph is disconnected.

### hasCycles

    for each vertex in graph
        add vertex to vertices
        
    until vertices is empty:
        ############################################
        ## start of new disconnected section      ##
        ############################################
        next is next from vertices
        remove next from vertices

        for each destination from next
            if destination not in stack
                add destination to stack
                remove destination from vertices
                
            
        ############################################
        ## depth first search of section          ##
        ############################################    
        prev = next;
                        
        next is next from vertices

        until section is processed:
            for destination from next:
                if destination not in vertices 
                    ## vertex has been visited before   
                    if directed
                        return true

                    ## not the previous vertex      
                    if not directed and destination not equals prev
                        return true;
                    
                else
                    add destination to stack
                    remove destination from vertices
                

            if stack is empty
                section processed
           
            prev = next;
            next = next in stack
    return false

## TimeComplexity

In every case each vertex in a graph is visited at most once, so time complexity in O(v + e), v being total vertex count, and e being total edge count.


## Correctness

Only problem might be dijkstras algorithm implementation, since I'm not quite sure if its correct. Tests pass, but since they are small, I'm not convinced yet.

## Learned things

Since I had never processed graphs before, this project was a interesting and teaching experience. Implementing these algorithms was quite easy with builtin Java stacks and queues.

Most notable thing learned during this project was detection of cycles and disconnections using DFS.
   
   
   
