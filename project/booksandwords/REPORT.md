# REPORT

## Description of code path if filepaths are valid

File is read in chunks for which a 1 MB buffer is allocated.
If the end of a word could not be found in the current chunk in memory, then some of the previous buffer is copied to the start of the buffer.

Each word is found is stored in a hashtable.
After the whole words-file is processed, then ignore-words file is processed in the same manner using a different hashtable.
After both files have been processed, then count is set to 0 for ignored words found.

## Algorithms and datastructures
### Hashtable implementation
    
Hashtable is implemented as a linear array of words. Normally this would be a template type, but since I'm not familiar with Java, I decided to only implement a hashtable for words.
    
HashTable is expanded if count is half of tables capacity, which is relatively low fill amount, but this is to reduce time when collisions happens.
   In case of a collision, then current index is incremented by one, until a null object or a matching word is found, resulting in a worst case search of O(n / 2), n being unique words in the table.
By adding collisions to the same array and, for example, not into a linked list, occupied indices can be stored and used directly when sorting words by count. More on this at the sorting step.

#### HashFunction
```
for(int i = 0; i < this.word.length(); i++){
    this.hash += this.word.charAt(i);
    this.hash += this.hash << 1 * (i | 0x1011);
    this.hash ^= this.hash >> 10;
}
this.hash = Math.abs(this.hash);
```

Basic idea is that longer strings would get bigger indices. Resulting stats show that longest chains aren't that long (for this particular file).

```
Bulk.txt and ignore-words.txt
CPU: Intel i5-7200U (4) @ 3.100GHz
   
Total words :: 2378679    
Unique Words :: 97152        
Words to ignore :: 37
Ignored words in total :: 295903
Collisions :: 503346, longest 53
Processing took 596 ms  
```

Keeping collisions chains short is critical for performance. For every collision each string has to be compared until a match or null is found. If hashes do not match then search can't be stopped since collisions are added to the next free space in the table.

example :
table      
[0] A (actual index should be 0) <-- Add C at index 0, collision! find next free slot.  
[1] B (actual index should be 1)        
[2] null  
[3] null

results in ...

table      
[0] A (actual index should be 0)   
[1] B (actual index should be 1)         
[2] C (actual index should be 0)     
[3] null

searching C, which should be at index 0
table                            
[0] A (actual index should be 0) | 1.) not a match, go to next  
[1] B (actual index should be 1) | 2.) not a match, go to next     
[2] C (actual index should be 0) | 3.) match!  
[3] null


### RadixSort 256
Sorting is done using radixSort using base 256, which has a constant time complexity of O(4 * n), n being unique words and 4 being number of bytes in an int. RadixSort is cache friendly since it handles the sorted array linearly, and also it doesn't branch, resulting in great performance on modern processor architecture.

When word is added to the hashtable it's index into the hashtable is stored in usedIndices array. this is to reduce time when sorting most occuring words, since words can be accessed by index directly.



## Possible bugs and limitations:
-  File reader would fail if a word would be longer than 1 MB (1 048 576 characters), since new data wouldn't fit into the file buffer. 

- If a word is shorted than 2 characters, then it's not calculated, but it's not included in the ignored count either. Not sure if this should be the case.

- Max unique word count is limited to 1 << 30 or 1 073 741 824.

## Difficulties during the project.
Since I have mainly programmed in C/C++, Java OOP paradigm gave me a hard time, and not being sure when something was returned by value or reference. 

## Things learned / important topics during the project
- I had implemented hashtables and sorting algorithm before this course. Because of this finding a solution was quite easy.

- Calculating time complexity wasn't new to me either, but also not something I did as a common practice.

- Thinking about worst case performace relative to common case performance, and how often these cases would occur.


   

   
      
   
   

   
   
   


   








