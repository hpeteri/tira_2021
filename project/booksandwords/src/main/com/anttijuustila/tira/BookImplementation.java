package com.anttijuustila.tira;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;

import java.lang.reflect.Array;
public class BookImplementation implements Book{
    //////////////////////////////////////////////////////////////////////
     static private Boolean stringCompare(String a, String b){
        if(a.length() != b.length())
            return false;
        
        for(int i = 0; i < a.length(); i++)
            if(a.charAt(i) != b.charAt(i)) return false;
        
        return true;
    }
    
    //////////////////////////////////////////////////////////////////////
    private HashTable uniqueWords = new HashTable();
    private HashTable ignoredWords = new HashTable();
    
    private int totalWordCount;
    private int totalIgnoredCount;
    private int uniqueWordCount;
    
    private String bookFileName;
    private String ignoreFileName;
    
    @Override
    public void setSource(String fileName,
                          String ignoreWordsFile) throws FileNotFoundException{
        File bookFile = new File(fileName);
        File ignoreFile = new File(ignoreWordsFile);
        
        if(!bookFile.exists() ||
           !ignoreFile.exists()){
            this.bookFileName = null;
            this.ignoreFileName = null;
            
            throw new FileNotFoundException("File not found");    
        }
        
        this.bookFileName = fileName;
        this.ignoreFileName = ignoreWordsFile;
    }
    static private void processFile(String fileName, HashTable table) throws IOException{
        File fileHandle = new File(fileName);
        int fileBufferSize = 1024 * 1024;
        char[] fileBuffer = null;
        try{
            fileBuffer = (char []) Array.newInstance(char.class, fileBufferSize);
        }catch(OutOfMemoryError e){
            System.out.println("Failed to create file buffer");
            throw e;
        }
        
        FileReader fr = new FileReader(fileHandle, StandardCharsets.UTF_8);
        
        long bytes = fileHandle.length();
        int bufferOffset = 0;
        for(;;){
            long read = 0;
            try{
                read = fr.read(fileBuffer,
                               bufferOffset,
                               fileBufferSize - bufferOffset); 
            }catch(Exception e){
                System.out.println("Failed to read from file");
            }
            if(read == -1)
                break;

            read += bufferOffset;
            
            int c0 = 0; 
            int c1 = -1;
            for(;;){
                //Find next word in the buffer
                for(c0 = c1 + 1;c0 < read && !Character.isLetter(fileBuffer[c0]);c0++){}
                if(c0 == read){
                    bufferOffset = 0;
                    break;
                }
                
                for(c1 = c0 + 1;c1 < read && Character.isLetter(fileBuffer[c1]);c1++){}
                if(c1 == read){
                    
                    String word = new String(fileBuffer, c0, c1 - c0);
                    System.arraycopy(word.toCharArray(), 0, fileBuffer, 0, word.length());
                    bufferOffset = (int)c1 - c0;
                    
                    break;
                }

                if(c1 - c0 == 1) continue;
                
                String word = new String(fileBuffer, c0, c1 - c0).toLowerCase();
                Word it = table.findOrAdd(new Word(word));
            }
        }
        fr.close();
    }
    @Override
    public void countUniqueWords() throws IOException, OutOfMemoryError{
        if(this.bookFileName == null ||
           this.ignoreFileName == null){
            throw new IOException("Null file name");
        }
                
        BookImplementation.processFile(this.bookFileName, this.uniqueWords);
        BookImplementation.processFile(this.ignoreFileName, this.ignoredWords);

        this.totalWordCount = 0;
        int[] indices = this.uniqueWords.getUsedIndices(); 
        for(int i = 0; i < this.uniqueWords.getCount(); i++){
            
            Word word = this.uniqueWords.getAt(indices[i]);
            if(word == null)
                continue;
            
            if(this.ignoredWords.find(word) != null){
                this.totalIgnoredCount += word.count;
                word.count = 0; //@set count to 0 if it is ignored.
                continue;
            }
            
            this.totalWordCount += word.count;
            this.uniqueWordCount ++;
        }
    }
    
    @Override
    public void report(){
        this.uniqueWords.sortByCount();        

        int uniqueCount = this.uniqueWords.getCount();
        int[] arr = this.uniqueWords.getUsedIndices();
        for(int i = 1; i <= Math.min(100, uniqueCount); i++){
            Word it = this.uniqueWords.getAt(arr[uniqueCount - i]);
            if(it.count == 0)
                break;
            
            System.out.println(String.format("[%d] %s :: %d", i, it.word, it.count));
        }
        System.out.println(String.format("Total words :: %d\nUnique Words :: %d\nWords to ignore :: %d\nIgnored words in total :: %d",
                                         this.totalWordCount,
                                         this.uniqueWords.getCount(),
                                         this.ignoredWords.getCount(),
                                         this.totalIgnoredCount));
        System.out.println(String.format("Collisions :: %d, longest %d", this.uniqueWords.totalCollisions, this.uniqueWords.maxCollisionChain));
        
        
        
    };
    
    @Override
    public void close(){
        this.uniqueWords = null;
        this.ignoredWords = null;
    }

    @Override
    public int getUniqueWordCount(){
        return this.uniqueWordCount;
    }

    @Override
    public int getTotalWordCount(){
        return this.totalWordCount;
    }

    @Override
    public String getWordInListAt(int position){
        int uniqueCount = this.uniqueWords.getCount();
        
        if(position < 0 ||
           position >= uniqueCount ||
           position >= 100) return null;

        this.uniqueWords.sortByCount();        
        
        return this.uniqueWords.getAt(this.uniqueWords.getUsedIndices()[uniqueCount - position-1]).word;
    }

    @Override
    public int getWordCountInListAt(int position){
        int uniqueCount = this.uniqueWords.getCount();

        if(position < 0 ||
           position >= uniqueCount ||
           position >= 100) return -1;
        
        this.uniqueWords.sortByCount();        
        
        
        return this.uniqueWords.getAt(this.uniqueWords.getUsedIndices()[uniqueCount - position - 1]).count;
    }
}
