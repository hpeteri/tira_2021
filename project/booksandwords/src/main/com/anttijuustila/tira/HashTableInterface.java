package com.anttijuustila.tira;

public interface HashTableInterface<E>{
    public E add(E e) throws OutOfMemoryError;
    public E find(E e);
    public E findOrAdd(E e) throws OutOfMemoryError;        
    public int getSize();
    public E getAt(int index);
}
