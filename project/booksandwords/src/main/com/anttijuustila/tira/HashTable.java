package com.anttijuustila.tira;

import java.lang.reflect.Array;
import java.util.Arrays;

public class HashTable {
    
    private int size;
    private Word[] table;
    private int[] usedIndices;
    private int count;
    private int maxCount;
    private Boolean isOrderDirty; //changed when add is called
    public int totalCollisions = 0;
    public int maxCollisionChain = 0;
    
    public HashTable(){
        this.isOrderDirty = true;
        this.size = 64;
        this.table = (Word []) Array.newInstance(Word.class, this.size);
        this.usedIndices = (int []) Array.newInstance(int.class, this.size);
        this.maxCount = size >> 1;
    }
    private void expand() throws OutOfMemoryError{
        try{
            int oldSize = this.size;
            Word[] oldTable = this.table;

            this.size <<= 1;
            if(this.size < 0){
                this.size = ~(1 << 31);
                
                if(oldSize == this.size)
                    throw new OutOfMemoryError("max table size reached");
            }
            this.maxCount <<= 1;
            
            this.count = 0;
            this.table = (Word []) Array.newInstance(Word.class, this.size);
            this.usedIndices = (int []) Array.newInstance(int.class, this.size);
            
            for(Word it: oldTable){
                this.add(it);                
            }
        }catch(OutOfMemoryError e){
            throw e;
        }        
    }
    ///Doesn't increment count if e is already in the table
    private Word add(Word e) throws OutOfMemoryError{
        if(e == null) return null;

        if(this.count == this.maxCount){
            try{
                this.expand();
            }catch(OutOfMemoryError err){
                throw err;
            }
        }
        
        int hash = e.hashCode();
        int index = hash % this.size;
        int tries = 0;
        for(int i = 0; i < this.size; i++){
            Word it = this.table[(index + i) % this.size];
            
            if(it == null){
                this.isOrderDirty = true;
                this.table[(index + i) % this.size] = e;
                this.usedIndices[this.count ++] = (index + i) % this.size;
                return this.table[(index + i) % this.size];
            }
            tries ++;
            this.maxCollisionChain = Math.max(tries, this.maxCollisionChain);
            this.totalCollisions ++;
        }        
        
        return null;
    }
    public Word find(Word e){
        if(e == null) return null;
        
        int hash = e.hashCode();
        int index = hash % this.size;        
        for(int i = 0; i < this.size; i++){
            Word it = this.table[(index + i) % this.size];

            if(it == null)
                return null;

            if(it.equals(e))
                return this.table[(index + i) % this.size];

            this.totalCollisions ++;
        }        
        
        return null;
    }
    ///increments count if e is already in the table
    public Word findOrAdd(Word e) throws OutOfMemoryError{
        if(e == null) return null;
        
        int hash = e.hashCode();
        int index = hash % this.size;
        int tries = 0;
        for(int i = 0; i < this.size; i++){
            Word it = this.table[(index + i) % this.size];

            if(it == null){
                if(this.count == this.maxCount){
                    try{
                        return this.add(e);
                    }catch(OutOfMemoryError err){
                        throw err;
                    }
                    
                }else{
                    this.isOrderDirty = true;
                    this.table[(index + i) % this.size] = e;
                    this.usedIndices[this.count ++] = (index + i) % this.size;
                    return this.table[(index + i) % this.size];
                }
                
            }
            if(it.equals(e)){
                it.count ++;
                return it;
            }
            tries ++;
            this.maxCollisionChain = Math.max(tries, this.maxCollisionChain);
            this.totalCollisions ++;
        }                
        return null;
    }
    public int getSize(){
        return this.size;
    }
    public int getCount(){
        return this.count;
    }
    public Word getAt(int index){
        return this.table[index];
    }
    public int[] getUsedIndices(){
        return this.usedIndices;
    }
    public void sortByCount(){
        if(!this.isOrderDirty)
            return;

        this.isOrderDirty = false;
        
        int[] counts = new int[256];
        int[] prefixSum = new int[256];

        int[] swapArray = new int[this.count];
        for(int b = 0; b < Integer.SIZE / 8; b++){
            Arrays.fill(counts, 0);
            for(int i = 0; i < this.count; i++){
                counts[(this.table[this.usedIndices[i]].count >> (b * 8)) & 0xFF] ++;
            }

            int sum = 0;
            for(int i = 0; i < 256; i++){
                prefixSum[i] = sum;                
                sum += counts[i];
            }
            
            Arrays.fill(counts, 0);
            for(int i = 0; i < this.count; i++){
                int index = (this.table[this.usedIndices[i]].count >> (b * 8)) & 0xFF;
                swapArray[prefixSum[index] + counts[index]] = this.usedIndices[i];
                counts[index]++;
            }
            
            int[] temp = this.usedIndices;
            this.usedIndices = swapArray;
            swapArray = temp;
        }
    }
}
