package com.anttijuustila.tira;

public class Word{
    int hash = 0;
    int count = 0;
    String word = null;

    public Word(String string){
        this.word = string;
        this.count = 1;
    }
    
    public int hashCode(){
        if(this.hash != 0){
            return this.hash;
        }
        
        for(int i = 0; i < this.word.length(); i++){
            this.hash += this.word.charAt(i);
            this.hash += this.hash << (i | 0x1011);
            this.hash ^= this.hash >> 10;
        }
        
        this.hash = Math.abs(this.hash);

        return this.hash;
        
  }
      

    public Boolean equals(Word it){
        if(this.hash != it.hash){
            return false;
        }

        return this.word.equals(it.word);
    }

}
